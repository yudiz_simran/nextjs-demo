import Link from 'next/Link'

const Navbar = () => {
    return (
        <div>
            <nav>
                <div>
                    <h1>Navbar</h1>
                </div>
                <Link href="/" ><a className="a-link" >Home</a></Link>
                <Link href="/about"  ><a className="a-link" >About</a></Link>
                <Link href="/list/list" ><a className="a-link" >List</a></Link>
            </nav>
            <hr />
        </div>
     );
}

export default Navbar;