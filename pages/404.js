import Link from 'next/link'
import { useEffect } from 'react';
import { useRouter } from 'next/router'

const NotFound = () => {

    const router = useRouter()

    useEffect(() => {
        console.log("UseEffect function called");
        setTimeout(() => {
            router.push("/")
        }, 3000);

    }, [])

    return (
        <div className="not-found">
            <h1>Oppps....</h1>
            <p>This page cannot be  found</p>
            <button className="btn btn-primary."><Link href="/"><a>Go back to Home</a></Link></button>
        </div>
     );
}

export default NotFound;