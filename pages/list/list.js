export const getStaticProps = async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/users')
    const data = await res.json()
    return {
        props: {data:data}
    }
}
const List  = ({ data }) => {
    return (
        <div>
           <h1>Users List:-</h1>

           {
               data.map((d) => {
                   return (
                       <>
                         <div key={d.id} >
                             <h4>{d.name}</h4>
                        </div>
                       </>
                   )

               })
           }
        </div>
     );
}

export default List;